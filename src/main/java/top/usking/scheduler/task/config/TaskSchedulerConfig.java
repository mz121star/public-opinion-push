package top.usking.scheduler.task.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启用定时器。
 *
 * @author shijin.liu
 * @since 1.0
 */
@Configuration
@EnableScheduling
public class TaskSchedulerConfig {
}
