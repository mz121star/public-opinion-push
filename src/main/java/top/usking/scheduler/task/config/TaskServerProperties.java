package top.usking.scheduler.task.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 参数配置类，从application.properties取值。
 *
 * @author shijin.liu
 * @since 1.0
 */
@Component
@ConfigurationProperties(prefix = "top.usking")
public class TaskServerProperties {
    private String sourceUrl;
    private String callbackUrl;
    private boolean callbackEnable;
    private boolean printDataEnable;

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public boolean getCallbackEnable() {
        return callbackEnable;
    }

    public void setCallbackEnable(boolean callbackEnable) {
        this.callbackEnable = callbackEnable;
    }

    public boolean getPrintDataEnable() {
        return printDataEnable;
    }

    public void setPrintDataEnable(boolean printDataEnable) {
        this.printDataEnable = printDataEnable;
    }
}
