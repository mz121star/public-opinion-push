package top.usking.scheduler.task.util;

import java.io.Serializable;
import java.util.Objects;

/**
 * 工具类，返回一个K,V的两个值的对象。
 *
 * @param <K> 左值。
 * @param <V> 右值。
 * @author shijin.liu
 * @since 1.0
 */
public final class Pair<K, V> implements Serializable {
    private static final long serialVersionUID = -6354241018957436055L;
    private final K left;
    private final V right;

    private Pair(final K left, final V right) {
        this.left = left;
        this.right = right;
    }

    public static <K, V> Pair<K, V> of(K key, V right) {
        return new Pair<>(key, right);
    }

    public K getLeft() {
        return left;
    }

    public V getRight() {
        return right;
    }


    @Override
    public int hashCode() {
        return left.hashCode() * 13 + (right == null ? 0 : right.hashCode());
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof Pair) {
            Pair pair = (Pair) obj;
            if (!Objects.equals(left, pair.left)) {
                return false;
            }
            if (!Objects.equals(right, pair.right)) {
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "(" + getLeft() + ',' + getRight() + ')';
    }
}
