package top.usking.scheduler.task.vo;

import java.io.Serializable;

public class BlogNumVo implements Serializable {

	private String trsId;

	private String publishTime;
	
	private String replyNum;
	
	public String getTrsId() {
		return trsId;
	}

	public void setTrsId(String trsId) {
		this.trsId = trsId;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public String getReplyNum() {
		return replyNum;
	}

	public void setReplyNum(String replyNum) {
		this.replyNum = replyNum;
	}
	
}
