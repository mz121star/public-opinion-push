package top.usking.scheduler.task.service;

import com.alibaba.fastjson.JSONObject;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Kafka 消息生产者类。
 *
 * @author shijin.liu
 * @since 1.0
 */
@Service
public class MessageProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageProducer.class);

    private final KafkaTemplate<String, JSONObject> kafkaTemplate;
    private final DataFetchService dataFetchService;

    public MessageProducer(KafkaTemplate<String, JSONObject> kafkaTemplate, DataFetchService dataFetchService) {
        this.kafkaTemplate = kafkaTemplate;
        this.dataFetchService = dataFetchService;
    }

    /**
     * 发送消息
     * kafkaTemplate.send(param...)是一个异步的方法，其发送结果可以通过Future的实现来获得。
     *
     * @param topicTypeEnum 推送的数据类型。
     * @param id            推送的ID.
     * @param message       推送的消息。
     */
    public void sendMessage(TopicTypeEnum topicTypeEnum, Long id, JSONObject message) {
        // 将消息发送到指定的主题下；这里可以是一个不存在的主题，会自动创建(注:自动创建的主题默认的分区数量是1个)
        ListenableFuture<SendResult<String, JSONObject>> test = kafkaTemplate.send(topicTypeEnum.getTopic(), message);
        test.addCallback(new ListenableFutureCallback<SendResult<String, JSONObject>>() {
            @Override
            public void onSuccess(SendResult<String, JSONObject> result) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SSS");
                RecordMetadata data = result.getRecordMetadata();
                LOGGER.info("-----> Send message to [{}] topic. Timestamp: {}({}).Data: {}",
                        data.topic(), data.timestamp(), dateFormat.format(data.timestamp()), message);
                //推送成功，调用回调接口，通知第三方推送成功。
//TODO                dataFetchService.sendCallbackMessage(id);
            }

            @Override
            public void onFailure(Throwable ex) {
                LOGGER.error("-----> Send message to [{}] topic failure.", topicTypeEnum.getTopic(), ex.getMessage());
            }
        });
    }
}
