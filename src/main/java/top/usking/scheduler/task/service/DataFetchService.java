package top.usking.scheduler.task.service;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import top.usking.scheduler.task.client.DataFetchClient;
import top.usking.scheduler.task.config.TaskServerProperties;
import top.usking.scheduler.task.util.Pair;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 *
 */
@Service
public class DataFetchService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataFetchService.class);
    public static final Long ZERO = new Long(0);

    private final TaskServerProperties taskServerProperties;
    private final DataFetchClient dataFetchClient;

    public DataFetchService(TaskServerProperties taskServerProperties, DataFetchClient dataFetchClient) {
        this.taskServerProperties = taskServerProperties;
        this.dataFetchClient = dataFetchClient;
    }

    public Pair<Long, JSONArray> fetchData(TopicTypeEnum topicType) {
        ResponseEntity<String> entity = dataFetchClient.getFetchData(topicType);
        String body = entity.getBody();
        LOGGER.info("-----> Status code: {}", entity.getStatusCode());
        JSONObject jsonObject = JSONObject.parseObject(body);
        Long status = jsonObject.getLong("status");
        if (Objects.equals(status, ZERO)) {
            String msg = jsonObject.getString("msg");
            LOGGER.warn("-----> Pulling data from [{}}] failure. [status:{},msg:{}]", topicType.getType(), status, msg);
            return null;
        }
        Long id = jsonObject.getLong("id");
        JSONArray data = jsonObject.getJSONArray("data");
        if (taskServerProperties.getPrintDataEnable()) {
            LOGGER.info("-----> Id: {},Type: {},Data: {}", id, topicType.getType(), data);
        }
        return Pair.of(id, data);
    }

    public void sendCallbackMessage(Long id) {
        if (taskServerProperties.getCallbackEnable()) {
            try {
                ResponseEntity<String> entity = dataFetchClient.callback(id);
                HttpStatus statusCode = entity.getStatusCode();
                LOGGER.info("-----> Send callback successful.[Code: {},message: {}]",
                        statusCode.value(), statusCode.getReasonPhrase());
            } catch (Exception e) {
                LOGGER.error("-----> Send callback message failure. message: {}", e.getMessage());
            }
        }
    }
}
