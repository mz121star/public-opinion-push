package top.usking.scheduler.task.service;

/**
 * 拉取数据类型与Kafka topic映射枚举类。
 *
 * @author shijin.liu
 * @since 1.0
 */
public enum TopicTypeEnum {
    /**
     * 网站
     */
    WEBSITE("forum", "website"),
    /**
     * 微博
     */
    WEIBO("weibo", "weibo"),
    /**
     * 社区信息
     */
    BLOG("blog", "blog"),
    /**
     * 微信
     */
    WECHAT("wechat", "wechat"),
    /**
     * 国外
     */
    ABROAD("abroad", "abroad"),
    /**
     * 百度指数
     */
    BD_INDEX("bdindex", "bdindex"),
    /**
     * 微信更新
     */
    WECHAT_NUM("wechatnum", "wechatnum"),
    /**
     * 微博更新
     */
    WEBO_NUM("webonum", "webonum"),
    /**
     * 社区信息更新
     */
    BLOG_NUM("blognum", "blognum");

    private String type;
    private String topic;

    TopicTypeEnum(String type, String topic) {
        this.type = type;
        this.topic = topic;
    }

    public String getType() {
        return type;
    }

    public String getTopic() {
        return topic;
    }
}
