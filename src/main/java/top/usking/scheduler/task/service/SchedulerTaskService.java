package top.usking.scheduler.task.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import top.usking.scheduler.task.client.DataFetchClient;
import top.usking.scheduler.task.config.TaskServerProperties;
import top.usking.scheduler.task.util.Pair;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Objects;

/**
 * 定时任务类。
 *
 * @author shijin.liu
 * @since 1.0
 */
@Service
public class SchedulerTaskService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerTaskService.class);

    private final DataFetchService dataFetchService;
    private final MessageProducer messageProducer;
    private final TaskServerProperties taskServerProperties;

    public SchedulerTaskService(DataFetchService dataFetchService, MessageProducer messageProducer,
                                TaskServerProperties taskServerProperties) {
        this.dataFetchService = dataFetchService;
        this.messageProducer = messageProducer;
        this.taskServerProperties = taskServerProperties;
    }

    /**
     * 定时器。
     * 单位是ms。
     */
    @Scheduled(cron = "${top.usking.scheduler.task.cron}")
    public void task() {
        LOGGER.info("-----> Scheduler task is running.");
        Arrays.stream(TopicTypeEnum.values())
                .filter(topicType -> !topicType.getTopic().equals(TopicTypeEnum.BD_INDEX.getTopic()))
                .forEach(topicType -> push2Topic(topicType));
        LOGGER.info("-----> Scheduler task is stopped.");
    }

    /**
     * 定时器。
     * 单位是ms。
     */
    @Scheduled(cron = "${top.usking.scheduler.bdindex.task.cron}")
    public void taskBdIndex() {
        LOGGER.info("-----> Scheduler task is running.");
        final TopicTypeEnum topicType = TopicTypeEnum.BD_INDEX;
        push2Topic(topicType);
        LOGGER.info("-----> Scheduler task is stopped.");
    }

    private synchronized void push2Topic(TopicTypeEnum topicType) {
        try {
            LOGGER.info("-----> Pulling data from [{}] type.", topicType.getType());
            //拉取数据
            Pair<Long, JSONArray> result = dataFetchService.fetchData(topicType);
            if (result == null) {
                //为空直接返回，不做任务操作。
                return;
            }
            Long id = result.getLeft();
            JSONArray data = result.getRight();
            LOGGER.info("-----> Scheduler task. [id: {}({}),data size: {}]", id, topicType.getType(), data.size());
            LOGGER.info("-----> Pushing data to [{}] topic.", topicType.getTopic());
            Iterator<Object> iterator = data.listIterator();
            while (iterator.hasNext()) {
                JSONObject jsonString = (JSONObject) iterator.next();
                if (taskServerProperties.getPrintDataEnable()) {
                    LOGGER.info("-----> Pushing message [{}].", jsonString.toString());
                }
                //向Kafka推送消息。
                messageProducer.sendMessage(topicType, id, jsonString);
                break;
            }
            //TODO 推送成功，调用回调接口，通知第三方推送成功。
            dataFetchService.sendCallbackMessage(id);
        } catch (Exception e) {
            LOGGER.error("-----> Scheduler [{}] task failure. message:{}", topicType.getType(), e.getMessage());
        }

    }

}
