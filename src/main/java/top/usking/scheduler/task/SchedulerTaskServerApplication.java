package top.usking.scheduler.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import top.usking.scheduler.task.service.DataFetchService;

import java.util.Map;

/**
 * 主程序入口类。
 *
 * @author shijin.liu
 * @since 1.0
 */
@SpringBootApplication
public class SchedulerTaskServerApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerTaskServerApplication.class);

    public static void main(String[] args) {
//        systemPropertiesConfig();
        ConfigurableApplicationContext context = SpringApplication.run(SchedulerTaskServerApplication.class, args);
        Map<String, Object> systemProperties = context.getEnvironment().getSystemProperties();
        systemProperties.forEach((key, value) -> {
            if (key.startsWith("java.security")) {
                LOGGER.info("-----> [{} : {}]", key, value);
            }
        });
    }

    /**
     * 系统环境属性 --- 设置
     * <p>
     * 注:因为是系统参数，多出地方都要使用；所以直接写在启动类里面
     * <p>
     * 设置系统环境属性 的 方式较多，这只是其中的一种
     * java -Djava.security.auth.login.config=/home/kafka/kafka_client_jaas.conf   -Djava.security.krb5.conf=/home/kafka/krb5.conf  -jar  springboot_kafka-0.0.1-SNAPSHOT.jar
     */
    private static void systemPropertiesConfig() {
        System.setProperty("java.security.auth.login.config", "/home/kerberos/kafka_client_jaas.conf");
        System.setProperty("java.security.krb5.config", "/home/kerberos/krb5.config");
    }
}