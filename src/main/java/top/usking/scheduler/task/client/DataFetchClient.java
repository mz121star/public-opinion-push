package top.usking.scheduler.task.client;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import top.usking.scheduler.task.config.TaskServerProperties;
import top.usking.scheduler.task.service.TopicTypeEnum;

/**
 * 调用第三方服务的接口。
 * 此类实现的方法有，取数据接口，推送成功回复接口。
 *
 * @author shijin.liu
 * @since 1.0
 */
@Component
public class DataFetchClient {
    private final RestTemplate restTemplate;
    private final TaskServerProperties taskServerProperties;

    public DataFetchClient(RestTemplate restTemplate, TaskServerProperties taskServerProperties) {
        this.restTemplate = restTemplate;
        this.taskServerProperties = taskServerProperties;
    }

    /**
     * 拉取数据。
     *
     * @param topicType 数据类型{@link TopicTypeEnum}.
     * @return 返回根据类型取到的数据。
     */
    public ResponseEntity<String> getFetchData(TopicTypeEnum topicType) {
        return restTemplate.getForEntity(taskServerProperties.getSourceUrl() + topicType.getType(), String.class);
    }

    /**
     * 推送数据到kafka成功后，通知第三方接口。
     *
     * @param id 推送成功数据的ID.
     * @return 应答数据。
     */
    public ResponseEntity<String> callback(Long id) {
        return restTemplate.getForEntity(taskServerProperties.getCallbackUrl() + id, String.class);
    }
}
